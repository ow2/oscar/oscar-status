# Pipelines status for OSCAR analysis

asm - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/asm/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/asm/-/commits/main)

authzforce - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/authzforce/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/authzforce/-/commits/main)

clif - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/clif/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/clif/-/commits/main)

docdoku - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/docdoku/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/docdoku/-/commits/main)

fusiondirectory - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/fusiondirectory/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/fusiondirectory/-/commits/main)

glpi - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/glpi/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/glpi/-/commits/main)

imixs-workflow - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/imixs-workflow/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/imixs-workflow/-/commits/main)

jeka - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/jeka/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/jeka/-/commits/main)

joram - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/joram/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/joram/-/commits/main)

knowage - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/knowage/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/knowage/-/commits/main)

lemonldap-ng - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/lemonldap-ng/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/lemonldap-ng/-/commits/main)

lutece - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/lutece/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/lutece/-/commits/main)

lsc - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/lsc/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/lsc/-/commits/main)

nuun - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/nuun/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/nuun/-/commits/main)

ocsinventory - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/ocsinventory/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/ocsinventory/-/commits/main)

prestashop - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/prestashop/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/prestashop/-/commits/main)

proactive - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/proactive/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/proactive/-/commits/main)

rocketchat - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/rocketchat/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/rocketchat/-/commits/main)

sat4j - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/sat4j/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/sat4j/-/commits/main)

seedstack - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/seedstack/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/seedstack/-/commits/main)

spoon - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/spoon/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/spoon/-/commits/main)

sympa - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/sympa/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/sympa/-/commits/main)

telosys - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/telosys/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/telosys/-/commits/main)

waarp - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/waarp/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/waarp/-/commits/main)

weblab - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/weblab/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/weblab/-/commits/main)

xwiki - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/xwiki/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/xwiki/-/commits/main)

zenroom - [![pipeline status](https://gitlab.ow2.org/ow2/oscar/zenroom/badges/main/pipeline.svg)](https://gitlab.ow2.org/ow2/oscar/zenroom/-/commits/main)
